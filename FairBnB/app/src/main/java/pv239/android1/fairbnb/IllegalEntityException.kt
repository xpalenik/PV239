package pv239.android1.fairbnb

/**
 * Created by Radim Koza on 23-Apr-19.
 * @author Radim Koza
 */
class IllegalEntityException : RuntimeException {

    /**
     * Creates a new instance of
     * `IllegalEntityException` without detail message.
     */
    constructor() {}

    /**
     * Constructs an instance of
     * `IllegalEntityException` with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    constructor(msg: String) : super(msg) {}

    /**
     * Constructs an instance of
     * `IllegalEntityException` with the specified detail
     * message and cause.
     *
     * @param message the detail message.
     * @param cause the cause
     */
    constructor(message: String, cause: Throwable) : super(message, cause) {}

}