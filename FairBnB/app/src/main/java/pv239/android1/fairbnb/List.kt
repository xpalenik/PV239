package pv239.android1.fairbnb

import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_list.*
import java.util.*
import kotlin.collections.ArrayList

class List : AppCompatActivity() {

    val locations = ArrayList<Location>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)



        val recyclerView = findViewById(R.id.recyclerView) as RecyclerView

        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        //Starting data
        locations.add( Location(50.6, 50.7, "Byt 3+kk ", 1200.0, "Nabízím pronájem pěkného bytu v cihlovém domě o dispozici 3 + kk a podlahové ploše 87 m2 včetně 2 balkónů",
            Date(2019, 9, 1), Date(2019, 9, 7), true, true))
        locations.add( Location(50.6, 50.7, "garsoniera", 999.9, "Nabízím dlouhodobý pronájem garsoniery o výměře 32 m2 s příslušenstvím",
            Date(2019, 9, 1), Date(2019, 11, 31), false, false))
        locations.add( Location(50.6, 50.7, "Pronájem bytu 2+1", 1100.0, "Pronájmu starší zařízený byt 1.kat. v 2. nadzemním podlaží o rozloze 56 m2",
            Date(2019, 5, 31), Date(2019, 6, 6), false, true))
        locations.add( Location(50.6, 50.7, "1+kk v centru", 749.99, "Nabízíme k pronájmu zcela nový byt 1+kk (47m2) v novostavbě z konce roku 2018",
            Date(2019, 5, 14), Date(2019, 9, 7), true, false))
        locations.add( Location(50.5, 50.5, "Vila Tugenhad", 9999.99, "Prozijte noc ve Vile Tugenhad",
            Date(2019, 7, 22), Date(2019, 8, 1), false, true))
        locations.add( Location(50.6, 50.7, "Pronájem", 800.0, "Hledám nutne pronájem pro 2 osoby",
            Date(2019, 6, 24), Date(2019, 9, 7), true, true))
        locations.add( Location(50.6, 50.7, "Spilberg Kazematy", 200.0, "Try out life as it was (organized by TIC Brno)",
            Date(2019, 3, 1), Date(2019, 11, 7), false, false))
        locations.add( Location(50.6, 50.7, "Pronájem Bytu 3+kk", 1700.0, "Dlouhodobě pronajmeme byt 3+kk o rozloze 74m2 v prvním podlaží",
            Date(2019, 0, 1), Date(2019, 11, 31), true, true))



        val selected_date = Date(intent.getIntExtra("year", 1970), intent.getIntExtra("month",1), intent.getIntExtra("day",1))
        println(selected_date)
        val selected_smoker = intent.getBooleanExtra("smoking", false)
        val selected_doubleroom = intent.getBooleanExtra("doubleroom", false)

        var filtered_locations = ArrayList<Location>()

        locations.forEach{
            if((it.doubleRoom == selected_doubleroom) and (!selected_smoker or it.smoking) and (it.startDate.before(selected_date) and it.endDate.after(selected_date))){
                    filtered_locations.add(it)
            }
        }
        val adapter = CustomAdapter(filtered_locations)

        recyclerView.adapter = adapter
    }
}
