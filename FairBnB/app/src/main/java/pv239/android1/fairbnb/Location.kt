package pv239.android1.fairbnb

import java.io.FileDescriptor
import java.time.LocalDate
import java.util.*

/**
 * Created by Radim Koza on 19-Apr-19.
 * @author Radim Koza
 */
class Location {
    var longitude: Double = 0.toDouble()
    var latitude: Double = 0.toDouble()
    var name: String? = null
    var price: Double = 0.toDouble()
    var description: String? = null
    var startDate: Date = Date()
    var endDate: Date = Date()
    var smoking : Boolean = false
    var doubleRoom : Boolean = false

    constructor(longitude : Double, latitude : Double, name : String, price : Double, description : String, startDate : Date, endDate : Date, smoking : Boolean, doubleRoom : Boolean){
        this.longitude = longitude
        this.latitude = latitude
        this.name = name
        this.price = price
        this.description = description
        this.startDate = startDate
        this.endDate = endDate
        this.smoking = smoking
        this.doubleRoom = doubleRoom
    }

    override fun toString(): String {
        return "Location(longitude=$longitude, latitude=$latitude, name=$name, price=$price, description=$description, startDate=$startDate, endDate=$endDate, smoking=$smoking, doubleRoom=$doubleRoom)"
    }


}
