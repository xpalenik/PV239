package pv239.android1.fairbnb

import java.io.IOException
import java.io.InputStreamReader
import java.net.URL
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement
import java.util.logging.Level
import java.util.logging.Logger
import javax.sql.DataSource

/**
 * DB tools.
 *
 * @author Radim Koza on 23-Apr-19
 */
object DBUtils {

    private val logger = Logger.getLogger(
        DBUtils::class.java.name
    )

    /**
     * Closes connection and logs possible error.
     *
     *
     * @param conn connection to close
     * @param statements  statements to close
     */
    fun closeQuietly(conn: Connection?, vararg statements: Statement) {
        for (st in statements) {
            try {
                st.close()
            } catch (ex: SQLException) {
                logger.log(Level.SEVERE, "Error when closing statement", ex)
            }
        }
        if (conn != null) {
            try {
                conn.autoCommit = true
            } catch (ex: SQLException) {
                logger.log(Level.SEVERE, "Error when switching autocommit mode back to true", ex)
            }

            try {
                conn.close()
            } catch (ex: SQLException) {
                logger.log(Level.SEVERE, "Error when closing connection", ex)
            }

        }
    }

    /**
     * Rolls back transaction and logs possible error.
     *
     *
     * @param conn connection
     */
    fun doRollbackQuietly(conn: Connection?) {
        if (conn != null) {
            try {
                if (conn.autoCommit) {
                    throw IllegalStateException("Connection is in the autocommit mode!")
                }
                conn.rollback()
            } catch (ex: SQLException) {
                logger.log(Level.SEVERE, "Error when doing rollback", ex)
            }

        }
    }

    /**
     * Extract key from given ResultSet.
     *
     *
     * @param key resultSet with key
     * @return key from given result set
     * @throws SQLException when operation fails
     */
    @Throws(SQLException::class)
    fun getId(key: ResultSet): Long? {
        if (key.metaData.columnCount != 1) {
            throw IllegalArgumentException("Given ResultSet contains more columns")
        }
        if (key.next()) {
            val result = key.getLong(1)
            if (key.next()) {
                throw IllegalArgumentException("Given ResultSet contains more rows")
            }
            return result
        } else {
            throw IllegalArgumentException("Given ResultSet contain no rows")
        }
    }

    /**
     * Reads SQL statements from file. SQL commands in file must be separated by
     * a semicolon.
     *
     * @param url url of the file
     * @return array of command  strings
     */
    private fun readSqlStatements(url: URL): Array<String> {
        try {
            val buffer = CharArray(256)
            val result = StringBuilder()
            val reader = InputStreamReader(url.openStream(), "UTF-8")
            while (true) {
                val count = reader.read(buffer)
                if (count < 0) {
                    break
                }
                result.append(buffer, 0, count)
            }
            return result.toString().split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        } catch (ex: IOException) {
            throw RuntimeException("Cannot read $url", ex)
        }

    }

    /**
     * Executes SQL script.
     *
     * @param ds datasource
     * @param scriptUrl url of PaymentManager.main.sql script to be executed
     * @throws SQLException when operation fails
     */
    @Throws(SQLException::class)
    fun executeSqlScript(ds: DataSource, scriptUrl: URL) {
        var conn: Connection? = null
        try {
            conn = ds.connection
            for (sqlStatement in readSqlStatements(scriptUrl)) {
                if (!sqlStatement.trim { it <= ' ' }.isEmpty()) {
                    conn!!.prepareStatement(sqlStatement).executeUpdate()
                }
            }
        } finally {
            closeQuietly(conn)
        }
    }

    /**
     * Check if updates count is one. Otherwise appropriate exception is thrown.
     *
     * @param count updates count.
     * @param entity updated entity (for includig to error message)
     * @param insert flag if performed operation was insert
     * @throws IllegalEntityException when updates count is zero, so updated entity does not exist
     * @throws ServiceFailureException when updates count is unexpected number
     */
    @Throws(IllegalEntityException::class, ServiceFailureException::class)
    fun checkUpdatesCount(
        count: Int, entity: Any,
        insert: Boolean
    ) {

        if (!insert && count == 0) {
            throw IllegalEntityException("Entity $entity does not exist in the db")
        }
        if (count != 1) {
            throw ServiceFailureException("Internal integrity error: Unexpected rows count in database affected: $count")
        }
    }

}
