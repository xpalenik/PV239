package pv239.android1.fairbnb

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.icu.util.Calendar
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_filters.*
import java.util.*

class Filters : AppCompatActivity() {

    var selected_day: Int? = null
    var selected_month: Int? = null
    var selected_year: Int? = null
    var doubleRoom : Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filters)

        btn_list_available.setOnClickListener{
            val intent = Intent(this, List::class.java)
            intent.putExtra("day", selected_day)
            intent.putExtra("month", selected_month)
            intent.putExtra("year", selected_year)
            intent.putExtra("doubleroom", doubleRoom)
            intent.putExtra("smoking", smoker.isChecked)
            startActivity(intent)
        }

        radioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                val radio: RadioButton = findViewById(checkedId)
                doubleRoom = (radio.text == "double")
            })

    }

    fun clickDataPicker(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in Toast
            Toast.makeText(this, """$dayOfMonth. ${monthOfYear + 1}. $year""", Toast.LENGTH_LONG).show()
            selected_year = year
            selected_month = monthOfYear + 1
            selected_day = dayOfMonth
        }, year, month, day)
        dpd.show()
    }

    fun radioButtonClicked(view: View){
        val radio: RadioButton = findViewById(radioGroup.checkedRadioButtonId)
        Toast.makeText(this,"On click : ${radio.text}",
            Toast.LENGTH_SHORT).show()
    }
}
