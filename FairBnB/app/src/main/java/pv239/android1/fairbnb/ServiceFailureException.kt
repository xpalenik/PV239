package pv239.android1.fairbnb

/**
 * Created by Radim on 23-Apr-19.
 * @author Radim Koza
 */
class ServiceFailureException : RuntimeException {
    /**
     * Constructs an instance of
     * `ServiceFailureException` with the specified detail
     * message and cause.
     *
     * @param msg the detail message.
     * @param cause the cause
     */
    constructor(msg: String, cause: Throwable) : super(msg, cause) {}

    /**
     * Constructs an instance of
     * `ServiceFailureException` with the specified detail
     * message.
     *
     * @param msg the detail message.
     */
    constructor(msg: String) : super(msg) {}

    /**
     * Creates a new instance of
     * `ServiceFailureException` without detail message.
     */
    constructor() {}
}
