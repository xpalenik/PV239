package pv239.android1.fairbnb

import javax.sql.DataSource
import java.sql.*
import java.time.LocalDate
import java.util.ArrayList
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Created by Radim Koza on 19-Apr-19.
 * @author Radim Koza
 */
class LocationManager {

    /*private var dataSource: DataSource? = null

    fun setDataSource(dataSource: DataSource) {
        this.dataSource = dataSource
    }

    private fun checkDataSource() {
        if (dataSource == null) {
            throw IllegalStateException("dataSource is not set.")
        }
    }

    @Throws(ServiceFailureException::class)
    fun createLocation(location: Location) {
        checkDataSource()
        if (location.id != null) {
            throw IllegalEntityException("location id is already set.")
        }
        var connection: Connection? = null
        var statement: PreparedStatement? = null
        try {
            connection = dataSource!!.connection
            connection!!.autoCommit = false
            statement = connection.prepareStatement(
                "INSERT INTO Invoice (name,longitude,latitude,description) " + "VALUES (?,?,?,?)",
                Statement.RETURN_GENERATED_KEYS
            )
            statement!!.setString(1, location.name)
            statement.setDouble(2, location.longitude)
            statement.setDouble(3, location.latitude)
            statement.setString(4, location.description)

            val count = statement.executeUpdate()
            DBUtils.checkUpdatesCount(count, location, true)

            val id = DBUtils.getId(statement.generatedKeys)
            location.id = id
            connection.commit()
        } catch (ex: SQLException) {
            val msg = "Error when inserting location into db"
            logger.log(Level.SEVERE, msg, ex)
            throw ServiceFailureException(msg, ex)

        } finally {
            DBUtils.doRollbackQuietly(connection)
            DBUtils.closeQuietly(connection, statement!!)
        }

    }

    @Throws(ServiceFailureException::class)
    fun deleteLocation(location: Location?) {
        checkDataSource()
        if (location == null) {
            throw IllegalArgumentException("location is null")
        }
        var conn: Connection? = null
        var st: PreparedStatement? = null
        try {
            conn = dataSource!!.connection
            conn!!.autoCommit = false

            st = conn.prepareStatement(
                "DELETE FROM Invoice WHERE id = ?"
            )
            st!!.setLong(1, location.id!!)

            val count = st.executeUpdate()
            DBUtils.checkUpdatesCount(count, location, false)
            conn.commit()

        } catch (ex: SQLException) {
            val msg = "Error when deleting location from the db"
            logger.log(Level.SEVERE, msg, ex)
            throw ServiceFailureException(msg, ex)
        } finally {
            DBUtils.doRollbackQuietly(conn)
            DBUtils.closeQuietly(conn, st!!)
        }
    }

    @Throws(ServiceFailureException::class)
    fun findLocation(id: Long?): Location? {
        checkDataSource()
        if (id == null) {
            throw IllegalArgumentException("id is null.")
        }
        var conn: Connection? = null
        var st: PreparedStatement? = null

        try {
            conn = dataSource!!.connection

            st = conn!!.prepareStatement(
                "SELECT id, name, longitude, latitude, description FROM Invoice WHERE id = ?"
            )
            st!!.setLong(1, id)

            return executeQueryForSingleInvoices(st)
        } catch (ex: SQLException) {
            val msg = "Error when getting invoice with id = $id from DB"
            logger.log(Level.SEVERE, msg, ex)
            throw ServiceFailureException(msg, ex)
        } finally {
            DBUtils.closeQuietly(conn, st!!)
        }
    }

    @Throws(ServiceFailureException::class)
    fun listAllLocations(): ArrayList<Location> {
        logger.info("Invoked listAllLocations")
        checkDataSource()
        var conn: Connection? = null
        var st: PreparedStatement? = null
        try {
            conn = dataSource!!.connection
            st = conn!!.prepareStatement(
                "SELECT id, name, longitude, latitude, description FROM Invoice"
            )

            return executeQueryForMultipleInvoices(st!!)
        } catch (ex: SQLException) {
            val msg = "Error when getting all invoices from DB"
            logger.log(Level.SEVERE, msg, ex)
            throw ServiceFailureException(msg, ex)

        } finally {
            DBUtils.closeQuietly(conn, st!!)
        }
    }

    companion object {

        private val logger = Logger.getLogger(
            LocationManager::class.java.name
        )

        @Throws(SQLException::class, ServiceFailureException::class)
        internal fun executeQueryForSingleInvoices(st: PreparedStatement): Location? {
            val rs = st.executeQuery()
            if (rs.next()) {
                val result = rowToBody(rs)
                if (rs.next()) {
                    throw ServiceFailureException(
                        "Internal integrity error: more invoices with the same id found!"
                    )
                }
                return result
            } else {
                return null
            }
        }

        @Throws(SQLException::class)
        internal fun executeQueryForMultipleInvoices(st: PreparedStatement): ArrayList<Location> {
            val rs = st.executeQuery()
            val result = ArrayList<Location>()
            while (rs.next()) {
                result.add(rowToBody(rs))
            }
            return result
        }

        @Throws(SQLException::class)
        private fun rowToBody(rs: ResultSet): Location {
            val result = Location()
            result.id = rs.getLong("id")
            result.name = rs.getString("name")
            result.latitude = rs.getDouble("latitude")
            result.longitude = rs.getDouble("longitude")
            result.description = rs.getString("description")
            return result
        }
    }*/

}
