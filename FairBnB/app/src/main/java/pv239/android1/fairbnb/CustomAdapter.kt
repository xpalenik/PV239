package pv239.android1.fairbnb

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(val locationList: ArrayList<Location>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.list_item_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return locationList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val location: Location = locationList[position]

        holder.textViewName?.text = location.name
        holder.textViewDescription?.text = location.description
        holder.textViewPrice?.text = location.price.toString() + " CZK"
        holder.textViewDate?.text = location.startDate.day.toString() + "." + location.startDate.month + "." + location.startDate.year +
                " - " + location.endDate.day.toString() + "." + location.endDate.month + "." + location.endDate.year

        holder.textViewSmoker?.text = if (location.smoking) "Smoking allowed" else ""
        holder.textViewRoomType?.text = if (location.doubleRoom) "Double room" else "Single room"
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val textViewName = itemView.findViewById(R.id.Name) as TextView
        val textViewDescription = itemView.findViewById(R.id.Description) as TextView
        val textViewPrice = itemView.findViewById(R.id.Price) as TextView
        val textViewDate = itemView.findViewById(R.id.Date) as TextView
        val textViewSmoker = itemView.findViewById(R.id.Smoker) as TextView
        val textViewRoomType = itemView.findViewById(R.id.RoomType) as TextView
    }
}